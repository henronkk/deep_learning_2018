# Special course in Information Technology: Deep Learning
# Henri R�nkk� 2018

import tensorflow as tf
import numpy as np
from tensorflow.python.platform import gfile
from random import randint
import os
from scipy.misc import imsave
from matplotlib import pyplot as plt

def unpickle(file):
    import pickle
    with open(file, 'rb') as fo:
        dict = pickle.load(fo, encoding='bytes')
    return dict
  
def initWeight(shape):
    shape = tf.cast(shape, tf.int64)
    weights = tf.truncated_normal(shape,stddev=0.1)
    return tf.Variable(weights)

# start with 0.1 so reLu isnt always 0
def initBias(shape):
    bias = tf.constant(0.1,shape=shape)
    return tf.Variable(bias)

# the convolution with padding of 1 on each side, and moves by 1.
def conv2d(x,W):
    return tf.nn.conv2d(x,W,strides=[1,1,1,1],padding="SAME")

# max pooling basically shrinks it by 2x, taking the highest value on each feature.
def maxPool2d(x):
    return tf.nn.max_pool(x,ksize=[1,2,2,1],strides=[1,2,2,1],padding="SAME")

batchsize = 50;
imagesize = 32;
colors = 3;

sess = tf.InteractiveSession()

img = tf.placeholder("float",shape=[None,imagesize,imagesize,colors])
lbl = tf.placeholder("float",shape=[None,10])

#cifar10-model made in the previous assignment, converted to the right format to get the feature maps
#conv1
wConv1 = initWeight([5,5,colors,8])
bConv1 = initBias([8])
conv1 = conv2d(img, wConv1)
bias1 = conv1 + bConv1
relu1 = tf.nn.relu(bias1)

#conv2
wConv2 = initWeight([5,5,8,8])
bConv2 = initBias([8])
conv2 = conv2d(relu1, wConv2)
bias2 = conv2 + bConv2
relu2 = tf.nn.relu(bias2)

#conv3
wConv3 = initWeight([5,5,8,8])
bConv3 = initBias([8])
conv3 = conv2d(relu2, wConv3)
bias3 = conv3 + bConv3
relu3 = tf.nn.relu(bias3)

#maxpool1
pool1 = maxPool2d(relu3)

#conv4
wConv4 = initWeight([5,5,8,16])
bConv4 = initBias([16])
conv4 = conv2d(pool1, wConv4)
bias4 = conv4 + bConv4
relu4 = tf.nn.relu(bias4)

#conv5
wConv5 = initWeight([5,5,16,16])
bConv5 = initBias([16])
conv5 = conv2d(relu4, wConv5)
bias5 = conv5 + bConv5
relu5 = tf.nn.relu(bias5)

#conv6
wConv6 = initWeight([5,5,16,16])
bConv6 = initBias([16])
conv6 = conv2d(relu5, wConv6)
bias6 = conv6 + bConv6
relu6 = tf.nn.relu(bias6)

#maxpool2
pool2 = maxPool2d(relu6)

#conv7
wConv7 = initWeight([5,5,16,64])
bConv7 = initBias([64])
conv7 = conv2d(pool2, wConv7)
bias7 = conv7 + bConv7
relu7 = tf.nn.relu(bias7)

#conv8
wConv8 = initWeight([5,5,64,64])
bConv8 = initBias([64])
conv8 = conv2d(relu7, wConv8)
bias8 = conv8 + bConv8
relu8 = tf.nn.relu(bias8)

#conv9
wConv9 = initWeight([5,5,64,64])
bConv9 = initBias([64])
conv9 = conv2d(relu8, wConv9)
bias9 = conv9 + bConv9
relu9 = tf.nn.relu(bias9)

#maxpool3
pool3 = maxPool2d(relu9)

#fully connected 1024
wFc1 = initWeight([(imagesize/4) * (imagesize/4) * 64, 1024])
bFc1 = initBias([1024])

shape2 = tf.cast([-1, (imagesize/4) * (imagesize/4) *64], tf.int32)
pool2flat = tf.reshape(pool3, shape2)

fc1 = tf.matmul(pool2flat,wFc1) + bFc1
relu10 = tf.nn.relu(fc1)

keepProb = tf.placeholder("float");
drop = tf.nn.dropout(relu10,keepProb);

wFc2 = initWeight([1024,10])
bFc2 = initWeight([10])
# softmax converts individual probabilities to percentages
guesses = tf.nn.softmax(tf.matmul(drop, wFc2) + bFc2)
# how wrong it is
cross_entropy = -tf.reduce_sum(lbl*tf.log(guesses + 1e-9))
# theres a lot of tensorflow optimizers such as gradient descent
# adam is one of them
optimizer = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
# array of bools, checking if each guess was correct
correct_prediction = tf.equal(tf.argmax(guesses,1), tf.argmax(lbl,1))
# represent the correctness as a float [1,1,0,1] -> 0.75
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))

sess.run(tf.initialize_all_variables());

batch = unpickle("/tmp/cifar10_data/cifar-10-batches-py/test_batch")

validationData = batch[b'data'][555:batchsize+555]
validationRawLabel = batch[b'labels'][555:batchsize+555]
validationLabel = np.zeros((batchsize,10))
validationLabel[np.arange(batchsize),validationRawLabel] = 1
validationData = validationData/255.0
validationData = np.reshape(validationData,[-1,3,32,32])
validationData = np.swapaxes(validationData,1,3)

saver = tf.train.Saver()
#saver.restore(sess, tf.train.latest_checkpoint(os.getcwd()+"/training/"))
#saver.restore(sess, tf.train.latest_checkpoint('/tmp/cifar10_train'))
# train for 20000
# print mnistbatch[0].shape
def train():
    for i in range(20000):
        randomint = randint(0,10000 - batchsize - 1)
        trainingData = batch["data"][randomint:batchsize+randomint]
        rawlabel = batch["labels"][randomint:batchsize+randomint]
        trainingLabel = np.zeros((batchsize,10))
        trainingLabel[np.arange(batchsize),rawlabel] = 1
        trainingData = trainingData/255.0
        trainingData = np.reshape(trainingData,[-1,3,32,32])
        trainingData = np.swapaxes(trainingData,1,3)

        if i%10 == 0:
            train_accuracy = accuracy.eval(feed_dict={
            img: validationData, lbl: validationLabel, keepProb: 1.0})
            print("step %d, training accuracy %g"%(i, train_accuracy))

            if i%50 == 0:
                saver.save(sess, '/tmp/cifar10_train/train', global_step=i)

        optimizer.run(feed_dict={img: trainingData, lbl: trainingLabel, keepProb: 0.5})
        print(i)

def unpool(value, name='unpool'):
    """N-dimensional version of the unpooling operation from
    https://www.robots.ox.ac.uk/~vgg/rg/papers/Dosovitskiy_Learning_to_Generate_2015_CVPR_paper.pdf

    :param value: A Tensor of shape [b, d0, d1, ..., dn, ch]
    :return: A Tensor of shape [b, 2*d0, 2*d1, ..., 2*dn, ch]
    """
    with tf.name_scope(name) as scope:
        sh = value.get_shape().as_list()
        dim = len(sh[1:-1])
        out = (tf.reshape(value, [-1] + sh[-dim:]))
        for i in range(dim, 0, -1):
            out = tf.concat([out, out], i)
        out_size = [-1] + [s * 2 for s in sh[1:-1]] + [sh[-1]]
        out = tf.reshape(out, out_size, name=scope)
    return out

def display():
    print("displaying")

    batchsizeFeatures = 50
    imageIndex = 56

    inputImage = batch[b"data"][imageIndex:imageIndex+batchsizeFeatures]
    inputImage = inputImage/255.0
    inputImage = np.reshape(inputImage,[-1,3,32,32])
    inputImage = np.swapaxes(inputImage,1,3)

    inputLabel = np.zeros((batchsize,10))
    inputLabel[np.arange(1),batch[b"labels"][imageIndex:imageIndex+batchsizeFeatures]] = 1;
    # inputLabel = batch["labels"][54]


    # prints a given image


    # # saves pixel-representations of features from Conv4 layer-5
    # featuresReLu1 = tf.placeholder("float",[None,16,16,16])
    # unReLu = tf.nn.relu(featuresReLu1)
    # unBias = unReLu
    # unConv = tf.nn.conv2d_transpose(unBias, wConv4, output_shape=[batchsizeFeatures,int(imagesize/2),int(imagesize/2),8] , strides=[1,1,1,1], padding="SAME")
    # unPool = unpool(unConv)
    # unReLu = tf.nn.relu(unPool)
    # unBias = unReLu
    # activations1 = relu4.eval(feed_dict={img: inputImage, lbl: inputLabel, keepProb: 1.0})
    # print(np.shape(activations1))

    # # display features
    # for i in range(16):
        # isolated = activations1.copy()
        # isolated[:,:,:,:i] = 0
        # isolated[:,:,:,i+1:] = 0
        # print(np.shape(isolated))
        # totals = np.sum(isolated,axis=(1,2,3))
        # best = np.argmin(totals,axis=0)
        # #print(best)
        # pixelactive = unConv.eval(feed_dict={featuresReLu1: isolated})
        # print(len(pixelactive))
        # # totals = np.sum(pixelactive,axis=(1,2,3))
        # # best = np.argmax(totals,axis=0)
        # #best = 0
        # saveImage(pixelactive[best],"activ"+str(i)+".png")
        # saveImage(inputImage[best],"activ"+str(i)+"-base.png")

    # display same feature for many images
    # for i in xrange(batchsizeFeatures):
    #     isolated = activations1.copy()
    #     isolated[:,:,:,:6] = 0
    #     isolated[:,:,:,7:] = 0
    #     pixelactive = unConv.eval(feed_dict={featuresReLu1: isolated})
    #     totals = np.sum(pixelactive,axis=(1,2,3))
    #     best = np.argmax(totals,axis=0)
    #     saveImage(pixelactive[i],"activ"+str(i)+".png")
    #     saveImage(inputImage[i],"activ"+str(i)+"-base.png")



    #saves pixel-representations of features from conv4 = layer-5
    featuresReLu2 = tf.placeholder("float",[None,16,16,16])
    unReLu2 = tf.nn.relu(featuresReLu2)
    unBias2 = unReLu2
    unConv2 = tf.nn.conv2d_transpose(unBias2, wConv4, output_shape=[batchsizeFeatures,int(imagesize/2),int(imagesize/2),8] , strides=[1,1,1,1], padding="SAME")
    unPool = unpool(unConv2)
    unReLu = tf.nn.relu(unPool)
    unBias = unReLu
    unConv = tf.nn.conv2d_transpose(unBias, wConv1, output_shape=[batchsizeFeatures,imagesize,imagesize,colors] , strides=[1,1,1,1], padding="SAME")
    activations1 = relu4.eval(feed_dict={img: inputImage, lbl: inputLabel, keepProb: 1.0})
    print(np.shape(activations1))

    #display features
    for i in range(32):
        isolated = activations1.copy()
        isolated[:,:,:,:i] = 0
        isolated[:,:,:,i+1:] = 0
        pixelactive = unConv.eval(feed_dict={featuresReLu2: isolated})
        # totals = np.sum(pixelactive,axis=(1,2,3))
        # best = np.argmax(totals,axis=0)
        best = 0
        saveImage(pixelactive[best],"activ"+str(i)+".png")
        saveImage(inputImage[best],"activ"+str(i)+"-base.png")


    # display same feature for many images
    # for i in xrange(batchsizeFeatures):
    #     isolated = activations1.copy()
    #     isolated[:,:,:,:8] = 0
    #     isolated[:,:,:,9:] = 0
    #     pixelactive = unConv.eval(feed_dict={featuresReLu2: isolated})
    #     totals = np.sum(pixelactive,axis=(1,2,3))
    #     # best = np.argmax(totals,axis=0)
    #     # best = 0
    #     saveImage(pixelactive[i],"activ"+str(i)+".png")
    #     saveImage(inputImage[i],"activ"+str(i)+"-base.png")



def saveImage(inputImage, name):
    # red = inputImage[:1024]
    # green = inputImage[1024:2048]
    # blue = inputImage[2048:]
    # formatted = np.zeros([3,32,32])
    # formatted[0] = np.reshape(red,[32,32])
    # formatted[1] = np.reshape(green,[32,32])
    # formatted[2] = np.reshape(blue,[32,32])
    # final = np.swapaxes(formatted,0,2)/255;
    final = inputImage
    final = np.rot90(np.rot90(np.rot90(final)))
    imsave(name,final)


def main(argv=None):
    display()
    # train()

if __name__ == '__main__':
    tf.app.run()
